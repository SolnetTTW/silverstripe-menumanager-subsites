<?php

namespace Solnet\Subsites;

use HeyDay\MenuManager\MenuSet;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\DataQuery;
use SilverStripe\ORM\DB;
use SilverStripe\ORM\Queries\SQLSelect;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Subsites\Model\Subsite;
use SilverStripe\Subsites\State\SubsiteState;

/**
 * Enables having seperate menusets for each Subsite.
 */

class MenuSetExtension extends DataExtension
{
    private static $db = [
        'SubsiteID' => 'Int',
    ];

    public function updateCMSFields(FieldList $fields)
    {
        // Invisibly sets the current subsite on the menuset you're editing
        $fields->push(
            HiddenField::create(
                'SubsiteID',
                'SubsiteID',
                SubsiteState::singleton()->getSubsiteId()
            )
        );
    }

    /**
     * Creates default menu sets for all subsites as well as the main site
     */
    public function requireDefaultRecords()
    {
        parent::requireDefaultRecords();

        $defaultSetNames = $this->owner->config()->get('default_sets') ?: [];
        $subsites = Subsite::get();
        foreach ($subsites as $subsite) {
            foreach ($defaultSetNames as $name) {
                $existingRecord = MenuSet::get()
                    ->filter('Name', $name)
                    ->filter('SubsiteID', $subsite->ID)
                    ->first();

                if (!$existingRecord) {
                    $set = MenuSet::create();
                    $set->Name = $name;
                    $set->SubsiteID = $subsite->ID;
                    $set->write();

                    DB::alteration_message("MenuSet '$name' created for subsite '$subsite->Title'", 'created');
                }
            }
        }
    }

    /**
     * Update any requests to limit the results to the current site
     * @param SQLSelect $query
     * @param DataQuery $dataQuery
     */
    public function augmentSQL(SQLSelect $query, DataQuery $dataQuery = null)
    {
        if (Subsite::$disable_subsite_filter) {
            return;
        }
        if ($dataQuery && $dataQuery->getQueryParam('Subsite.filter') === false) {
            return;
        }

        // If you're querying by ID, ignore the sub-site
        if ($query->filtersOnID()) {
            return;
        }

        $subsiteID = null;
        if (Subsite::$force_subsite) {
            $subsiteID = Subsite::$force_subsite;
        } else {
            $subsiteID = SubsiteState::singleton()->getSubsiteId();
        }

        if ($subsiteID === null) {
            return;
        }

        $query->addWhere("\"MenuSet\".\"SubsiteID\" IN ($subsiteID)");
    }
}
