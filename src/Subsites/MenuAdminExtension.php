<?php

namespace Solnet\Subsites;

use SilverStripe\Core\Extension;
use SilverStripe\Subsites\State\SubsiteState;

/**
 * Ensures only those menu sets that are relevant to the current subsite are visible in the
 * Menu Manager CMS' ModelAdmin.
 */

class MenuAdminExtension extends Extension
{
    public function updateEditForm($form)
    {
        $gridField = $form->Fields()->fieldByName(
            //$this->owner->sanitiseClassName($this->owner->modelClass)
            str_replace('\\', '-', $this->owner->modelClass) // sanitiseClassName is a protected method
        );

        // Only affect menusets
        if ($this->owner->modelClass == 'Heyday\MenuManager\MenuSet') {
            // Filter list to only those for the current subsite
            $list = $gridField->getList()->filter(
                [
                    'SubsiteID' => SubsiteState::singleton()->getSubsiteId()
                ]
            );
            $gridField->setList($list);
        }

        // Regardless, sort menus by name
        $list = $gridField->getList()->sort('Name');
        $gridField->setList($list);
    }
}
